require 'test_helper'

class ChichenControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get galery" do
    get :galery
    assert_response :success
  end

  test "should get book" do
    get :book
    assert_response :success
  end

  test "should get tours" do
    get :tours
    assert_response :success
  end

  test "should get directions" do
    get :directions
    assert_response :success
  end

  test "should get visit" do
    get :visit
    assert_response :success
  end

end
