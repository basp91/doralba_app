class MeridaController < ApplicationController
  def home
  end

  def galery
  end

  def book
  end

  def tours
  end

  def directions
  end

  def visit
  end
end
